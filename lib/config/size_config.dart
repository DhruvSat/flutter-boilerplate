import 'package:flutter/widgets.dart';

class SizeConfig {
  static double? _screenWidth;
  static double? screenWidth;
  static double? screenHeight;
  static double? _screenHeight;
  static double _blockSizeHorizontal = 0;
  static double _blockSizeVertical = 0;

  static bool? portrait;
  static double? textMultiplier;
  static double? imageMultiplier;
  static double? heightMultiplier;
  static double? widthMultiplier;

  void init(BoxConstraints constraints) {
    _screenWidth = constraints.maxHeight;
    _screenHeight = constraints.maxWidth;

    _blockSizeHorizontal = (_screenWidth! / 100);
    _blockSizeVertical = (_screenHeight! / 100);

    screenWidth = _screenWidth;
    screenHeight = _screenHeight;
    textMultiplier = _blockSizeVertical;
    imageMultiplier = _blockSizeHorizontal;
    heightMultiplier = _blockSizeVertical;
    widthMultiplier = _blockSizeHorizontal;
  }
}
